#!/usr/bin/env sh


make_dirs()
{
    mkdir -p build
    mkdir -p build/css
    mkdir -p build/assets
    mkdir -p build/fonts
}


website_copy_static()
{
    # Copy static files
    cp -rv src/assets \
           build

    mkdir -p build/fonts
    cp -v submodules/plex/packages/plex-sans/fonts/complete/ttf/IBMPlexSans-Medium.ttf \
          submodules/plex/packages/plex-sans/fonts/complete/ttf/IBMPlexSans-SemiBold.ttf \
          submodules/plex/packages/plex-sans/fonts/complete/ttf/IBMPlexSans-Text.ttf \
          submodules/plex/packages/plex-sans/fonts/complete/woff/IBMPlexSans-Medium.woff \
          submodules/plex/packages/plex-sans/fonts/complete/woff/IBMPlexSans-SemiBold.woff \
          submodules/plex/packages/plex-sans/fonts/complete/woff/IBMPlexSans-Text.woff \
          submodules/plex/packages/plex-sans/fonts/complete/woff2/IBMPlexSans-Medium.woff2 \
          submodules/plex/packages/plex-sans/fonts/complete/woff2/IBMPlexSans-SemiBold.woff2 \
          submodules/plex/packages/plex-sans/fonts/complete/woff2/IBMPlexSans-Text.woff2 \
          build/fonts
}


website_build_css()
{
    scss_files="$(find src/scss -name '[^_]*.scss')"
    while IFS= read -r src; do
        dest="$(printf "%s" "${src}" | sed -e "s#src/scss#build/css#")"
        dest="${dest%%.*}.css"
        "${SASSC_BIN}" --load-path submodules "${src}" "${dest}"
    done <<EOF
${scss_files}
EOF
}


website_generate_blog_index()
{
    # Clear old index
    blog_index_src="src/markdown/blog/_index.md"
    blog_index_dest="src/markdown/blog/index.md"
    [ -e "${blog_index_dest}" ] && \
        rm -v "${blog_index_dest}"

    # Build blog index
    blog_md_files="$(find src/markdown/blog -name '[^_]*.md')"
    blog_md_count="0"

    # Grab all publishing dates of the blogs, sorted by latest to oldest
    blog_md=""
    while IFS= read -r src; do
        blog_md_count="$((blog_md_count + 1))"

        pub_date="$("${PANDOC_BIN}" --lua-filter="src/pandoc/date.lua" \
                                    "${src}")"
        title="$("${PANDOC_BIN}" --metadata="query:title" \
                                 --lua-filter="src/pandoc/meta.lua" \
                                 "${src}")"
        link_href="$(printf "%s" "${src}" \
            | sed -e 's#src/markdown/#/#;s#/index.md##')"

        _blog_md="${pub_date}:${title}:${link_href}"
        if [ "${blog_md}" ]; then
            blog_md="$(printf "%s\\n%s" "${blog_md}" "${_blog_md}")"
        else
            blog_md="${_blog_md}"
        fi
    done <<EOF
${blog_md_files}
EOF

    blog_md="$(printf "%s" "${blog_md}" | sort -n -r)"

    # Generate the blog index page
    cp -v "${blog_index_src}" "${blog_index_dest}"

    _date() {
        local result
        : "${result:=$(date -d"$1" "$2")}"
        : "${result:=$(date -jf "%Y-%m-%d" "$1" "$2")}"
        printf "%s" "$result"
    }

    current_group=""
    while IFS= read -r src; do
        _pub_date="$(printf "%s" "${src}" | cut -d':' -f1)"
        title="$(printf "%s" "${src}" | cut -d':' -f2)"
        link_href="$(printf "%s" "${src}" | cut -d':' -f3)"

        group="$(_date "${_pub_date}" "+%B %Y")"
        pub_date="$(_date "${_pub_date}" "+%a %d")"

        [ "${group}" != "${current_group}" ] && {
            # This blog is from a different month to the current group,
            # therefore we will end the current group and start a new one
            [ "${current_group}" ] && \
                printf "\\n</article>\\n" >> "${blog_index_dest}"
            printf "\\n\\n<article class=\"blog-index-group\">\\n## %s\\n" "${group}" >> "${blog_index_dest}"
            current_group="${group}"
        }
        printf "<div class=\"blog-index-entry\"><div class=\"blog-index-entry-date\">**%s**&nbsp;</div><div>[%s](%s)</div></div>\\n" "${pub_date}" "${title}" "${link_href}" >> "${blog_index_dest}"
    done << EOF
${blog_md}
EOF

    printf "</article>\\n" >> "${blog_index_dest}"
}


website_build_pages()
{
    # Build pages
    md_files="$(find src/markdown -name '[^_]*.md')"
    while IFS= read -r src; do
        gen_toc=""
        gen_wordstats=""
        format_date=""

        word_count=""
        read_time_min=""
        read_time_max=""
        date=""

        dest="$(printf "%s" "${src}" | sed -e "s#src/markdown#build#")"
        base_dir="${dest%"${dest##*/}"}"
        base_dir="${base_dir%/}"
        mkdir -p "${base_dir}"
        dest="${dest%%.*}.html"

        if [ "${dest}" = "build/index.html" ]; then
            title="Julian Heng"
        else
            title="$("${PANDOC_BIN}" --metadata="query:title" \
                                     --lua-filter="src/pandoc/meta.lua" \
                                     "${src}")"
        fi
        og_fname="$(printf "og_%s" "${title}" \
            | tr '[:upper:]' '[:lower:]' \
            | sed -e "s/\s/-/g")"
        src_base_dir="${src%"${src##*/}"}"
        src_base_dir="${src_base_dir%/}"
        og_html_dest="${src_base_dir}/_${og_fname}.html"
        og_img_path="${base_dir#build}/${og_fname}.png"
        og_img_path="${og_img_path#/}"

        case "${base_dir}" in
            *"blog/"[0-9][0-9][0-9][0-9]*)
                gen_toc="1"
                gen_wordstats="1"
                format_date="1"
            ;;
        esac

        [ "${gen_wordstats}" ] && \
            read -r word_count read_time_min read_time_max << EOF
$("${PANDOC_BIN}" --lua-filter="src/pandoc/wordcount.lua" "${src}")
EOF

        [ "${format_date}" ] && \
            date="$("${PANDOC_BIN}" --lua-filter="src/pandoc/date.lua" \
                                    --metadata="datefmt:%A, %d %B %Y" \
                                    "${src}")"

        # Generate OpenGraph Image
        "${PANDOC_BIN}" --metadata=title="${title}" \
                        --metadata=gen-wordstats="${gen_wordstats}" \
                        --metadata=word-count="${word_count}" \
                        --metadata=read-time-min="${read_time_min}" \
                        --metadata=read-time-max="${read_time_max}" \
                        --metadata=date="${date}" \
                        --output="${og_html_dest}" \
                        --standalone \
                        --template="src/html/og.html" \
                        --css="css/bootstrap.css" \
                        --css="css/fontawesome.css" \
                        --css="css/fonts.css" \
                        --css="css/layout.css" \
                        --css="css/light.css" <<EOF
EOF

        "${PANDOC_BIN}" "${src}" \
                        --lua-filter="src/pandoc/lazy_image.lua" \
                        --metadata=title-prefix="Julian Heng" \
                        --metadata=gen-toc="${gen_toc}" \
                        --metadata=gen-wordstats="${gen_wordstats}" \
                        --metadata=word-count="${word_count}" \
                        --metadata=read-time-min="${read_time_min}" \
                        --metadata=read-time-max="${read_time_max}" \
                        --metadata=date="${date}" \
                        --metadata=website-base-url="${WEBSITE_BASE_URL}" \
                        --metadata=og-img-path="${og_img_path}" \
                        --output="${dest}" \
                        --standalone \
                        --from=markdown \
                        --to=html \
                        --template="src/html/template.html" \
                        --include-before-body="src/html/navigation.html" \
                        --include-after-body="src/html/footer.html" \
                        --strip-comments \
                        --toc \
                        --css="css/bootstrap.css" \
                        --css="css/fontawesome.css" \
                        --css="css/fonts.css" \
                        --css="css/layout.css" \
                        --css="css/light.css"
    done <<EOF
${md_files}
EOF
}


website_render_opengraph_image()
{
    og_files="$(find src -name '_og_*.html')"
    while IFS= read -r src; do
        dest="$(printf "%s" "${src}" | sed -e "s/_og/og/;s/.html/.png/")"
        "${CAPTURE_WEBSITE_BIN}" "${src}" \
                                 --output "${dest}" \
                                 --width 1200 \
                                 --height 630 \
                                 --type png \
                                 --overwrite \
                                 --style "build/css/bootstrap.css" \
                                 --style "build/css/fontawesome.css" \
                                 --style "build/css/fonts.css" \
                                 --style "build/css/layout.css" \
                                 --style "build/css/light.css"
        "${PNGQUANT_BIN}" --ext ".png" --force "${dest}"
    done <<EOF
${og_files}
EOF
}


website_copy_blog_static()
{
    # Copy static files from blogs
    blog_img_files="$(find src/markdown/blog -type f -not -name '*.md' -and -not -name '*.html')"
    while IFS= read -r src; do
        dest="$(printf "%s" "${src}" | sed -e "s#src/markdown#build#")"
        cp -v "${src}" "${dest}"
    done <<EOF
${blog_img_files}
EOF
}


website_copy_opengraph_image()
{
    og_files="$(find src/markdown -type f -name 'og_*.png')"
    while IFS= read -r src; do
        dest="$(printf "%s" "${src}" | sed -e "s#src/markdown#build#")"
        cp -v "${src}" "${dest}"
    done <<EOF
${og_files}
EOF
}


website_build()
{
    website_copy_static
    website_build_css
    website_generate_blog_index
    website_build_pages
    website_render_opengraph_image
    website_copy_blog_static
    website_copy_opengraph_image
}


resume_build()
{
    mkdir -p submodules/resume/build
    "${LUALATEX_BIN}" --output-format=pdf \
                      --output-directory=submodules/resume/build \
                      --jobname=julian_heng_resume_2025 \
                      submodules/resume/resume.tex
}


fontawesome_copy()
{
    # Copy fontawesome font files
    cp -v submodules/fontawesome/webfonts/* \
          build/fonts
}


resume_copy()
{
    # Copy resume to assets
    cp -v submodules/resume/build/*.pdf \
          build/assets
}


purgecss_build()
{
    css_files="$(find build -name '*.css')"
    html_files="$(find build -name '*.html')"

    "${PURGECSS_BIN}" --css ${css_files} \
                      --content ${html_files} \
                      --output build/css
}


minify_build()
{
    "${MINIFY_BIN}" --recursive --sync --output build build
}


all()
{
    # Create output build folder
    make_dirs

    # Build website
    website_build

    # Build resume
    resume_build

    # Copy submodule build files
    fontawesome_copy
    resume_copy

    # Optimise and minify
    purgecss_build
    minify_build
}


clean()
{
    rm -rfv build src/markdown/blog/index.md submodules/resume/build
    og_files="$(find src -name '_og_*.html')"
    while IFS= read -r src; do
        rm -rfv "${src}"
    done <<EOF
${og_files}
EOF
}


serve()
{
    (
        cd build
        "${HTTP_SERVER_BIN}" -p 8000
    )
}


main()
{
    # Show all commands
    set -x

    # Setup environment
    # Programs
    : "${SASSC_BIN:=$(command -v sassc)}"
    : "${PANDOC_BIN:=$(command -v pandoc)}"
    : "${CAPTURE_WEBSITE_BIN:=$(command -v capture-website)}"
    : "${PNGQUANT_BIN:=$(command -v pngquant)}"
    : "${LUALATEX_BIN:=$(command -v lualatex)}"
    : "${PURGECSS_BIN:=$(command -v purgecss)}"
    : "${MINIFY_BIN:=$(command -v minify)}"
    : "${HTTP_SERVER_BIN:=$(command -v http-server)}"

    # Site config
    : "${WEBSITE_BASE_URL:="https://julian-heng.au"}"

    for target in "${@:-all}"; do
        "${target}"
    done
}


main "$@"
