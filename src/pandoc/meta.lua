function Meta(m)
    for k, v in pairs(m) do
        if type(v) == 'table' and k == m.query then
            local isInline = false
            if pandoc.utils.type then
                isInline = pandoc.utils.type(v) == 'Inlines'
            else
                isInline = v.t == "MetaInlines"
            end
            if isInline then
                print(pandoc.utils.stringify({table.unpack(v)}))
                os.exit(0)
            end
        end
    end

    io.stderr:write(string.format("query '%s' not found\n", m.query))
    os.exit(1)
end
