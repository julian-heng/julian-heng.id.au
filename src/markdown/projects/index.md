---
title: Projects
...

## [neosnip](https://gitlab.com/julian-heng/neosnip)

A simple screenshot application for Windows written in C.

This project started as a learning exercise of how ShareX creates a translucent
window screenshot. The original program was written in C#, which was then later
ported to C.


## [sys-line](https://gitlab.com/julian-heng/sys-line)

A simple status line generator written in Python 3.

This project was created to replace system info collection scripts that were
written using BASH in 2017. This was inspired by other projects such as
neofetch and inxi. The original BASH scripts initially supported macOS, with
Linux support added later on. The process of adding support for a new operating
system was awkward and cumbersome. Thus, it was rewritten in Python utilising
an object oriented design to allow for ease of extending to new operating
systems.

This project also contained a continuous integration pipeline to lint and test
on every push. This ensured that best practices were being followed and reduced
the amount of regressions in the project.


## [mp3pls](https://gitlab.com/julian-heng/mp3pls)

Batch MP3 converter written in BASH.

This project was used to batch convert a music library from a mix of FLAC and
WAV files into MP3 files. The result was a smaller music library that could be
copied to other devices.

Written in BASH and using FFmpeg for the conversion, this script is capable of
parallel execution through background tasks and is a robust framework to build
other converter tools.


## [advent-of-code-2020](https://www.gitlab.com/julian-heng/advent-of-code-2020)

Solutions to the advent of code 2020 puzzles.

The solutions were written in Python 3 and Golang. The main objective for this
project was to solve all of the programming puzzles throughout the month of
December, as well as to learn how to code in Golang.


## [advent-of-code-2020-react](https://julian-heng.gitlab.io/advent-of-code-2020-react)

Browser based application for solving the advent of code 2020 puzzles.

A Typescript-React port of the above advent of code 2020 puzzle solutions. This
allows anyone to solve the puzzles from within their browsers. The main
objective was to learn more about front-end web development as well as
Typescript.
